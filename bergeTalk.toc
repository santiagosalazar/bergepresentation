\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{Notation}{3}{0}{2}
\beamer@sectionintoc {3}{Definitions}{7}{0}{3}
\beamer@sectionintoc {4}{Berge Hypergraph example}{11}{0}{4}
\beamer@sectionintoc {5}{Identity Example}{14}{0}{5}
\beamer@sectionintoc {6}{General Results}{19}{0}{6}
\beamer@sectionintoc {7}{Downsets}{22}{0}{7}
\beamer@sectionintoc {8}{Shifting Example}{23}{0}{8}
\beamer@sectionintoc {9}{$F$ is a tree}{40}{0}{9}
\beamer@sectionintoc {10}{Complete Bipartite}{44}{0}{10}
\beamer@sectionintoc {11}{Column sum restriction}{46}{0}{11}
\beamer@sectionintoc {12}{$I_2\times I_t$}{50}{0}{12}
\beamer@sectionintoc {13}{I_3 X I_t}{54}{0}{13}
\beamer@sectionintoc {14}{General I_s X I_t}{57}{0}{14}
