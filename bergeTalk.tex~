%beamer latex file
\documentclass[notheorems]{beamer}
\usepackage{beamerthemesplit}
\usepackage{multirow , amsmath,amssymb, kbordermatrix}
\usepackage[utf8]{inputenc}
\usepackage{blkarray}


%\input colordvi.tex
%
%\setlength{\topmargin}{0.4cm} \setlength{\headheight}{-.4cm}
%\setlength{\headsep}{-1.5cm} \setlength{\textheight}{9.7in}
%\setlength{\oddsidemargin}{-1cm} \setlength{\textwidth}{7.5in}

%\newcommand{\donothing}[1]{}
\definecolor{darkgreen}{rgb}{0,.35,.3}
\DeclareFontFamily{U}{mathb}{\hyphenchar\font45}
\DeclareFontShape{U}{mathb}{m}{n}{
<5> <6> <7> <8> <9> <10> gen * mathb
<10.95> mathb10 <12> <14.4> <17.28> <20.74> <24.88> mathb12
}{}
\DeclareSymbolFont{mathb}{U}{mathb}{m}{n}
\DeclareMathSymbol{\llcurly}{3}{mathb}{"CE}
\DeclareMathSymbol{\ggcurly}{3}{mathb}{"CF}
\DeclareFontFamily{U}{matha}{\hyphenchar\font45}
\DeclareFontShape{U}{matha}{m}{n}{
<5> <6> <7> <8> <9> <10> gen * matha
<10.95> matha10 <12> <14.4> <17.28> <20.74> <24.88> matha12
}{}
\DeclareSymbolFont{matha}{U}{matha}{m}{n}
\def\proof{\noindent{\bf Proof: }}
\def\qed{ \hskip 20pt{\vrule height7pt width6pt depth0pt}\hfil}
\newcommand{\ncols}[1]{\|{#1}\|}
\newcommand{\darkgreen}{\textcolor{darkgreen}}
\newcommand{\tcdg}[1]{\textcolor{darkgreen}{#1}}
\def\bs{\llcurly}
\def\forb{{\mathrm{forb}}}
\def\bh{{\mathrm{ Bh}}}
\def\ex{{\mathrm{ex}}}
\def\Av{{\mathrm{Avoid}}}
\def\avbh{{\mathrm{BAvoid}}}
\newcommand{\0}{\mathbf{0}}
\newcommand{\1}{\mathbf{1}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\bS}{\mathbb{S}}
\newcommand{\sE}{{\mathcal E}}
\newcommand{\la}{\lambda}
\newcommand{\e}{\varepsilon}
\newcommand{\pd}{\partial}
\newcommand{\norm}[1]{\left\Vert #1 \right\Vert}
\newcommand{\bke}[1]{\left( #1 \right)}
\newcommand{\bkt}[1]{\left[ #1 \right]}
\newcommand{\bket}[1]{\left\{ #1 \right\}}
\renewcommand{\L}{{\mathcal{L}}}
\newcommand{\loc}{_\text{loc}}
\newcommand{\dis}{\displaystyle}
\newcommand{\lec}{{\ \lesssim \ }}
\newcommand{\cmskip}{\hspace*{1cm}}
\renewcommand{\Re}{\mathop{\mathrm{Re}}}
\renewcommand{\Im}{\mathop{\mathrm{Im}}}
\newcommand{\red}[1]{{\color{red}#1 }}

\newcommand{\Pc}{{\bf P_{\!c}\,}}

\newenvironment{E}{\begin{equation}}{\end{equation}}
\def\qed{\hfill\vrule height7pt width6pt depth0pt}
\def\forb{{\hbox{\it forb}}}
\def\Av{{\hbox{Avoid}}}
\def\defin{{\color{blue}{\bf Definition }}}
\def\theor{{\color{blue}{\bf Theorem }}}
\def\corol{{\color{blue}{\bf Corollary }}}
\def\conje{{\color{blue}{\bf Conjecture }}}
\def\probl{{\color{blue}{\bf Problem }}}
\def\prof{{\color{blue}{\bf Proof: }}}
\def\remark{{\color{blue}{\bf Remark }}}
\def\0{{\bf 0}}
\def\1{{\bf 1}}
\def\x{{\bf x}}
\def\redzero{{\color{red}0}}
\def\redone{{\color{red}1}}

 \newcommand{\linelessfrac}[2]{\genfrac{}{}{0pt}{}{#1}{#2}}
\renewcommand{\l}{\ell}

\newtheorem{thm}{{\color{blue}Theorem}}[section]
\newtheorem{prop}[thm]{{\color{blue}Proposition}}
\newtheorem{cor}[thm]{{\color{blue}Corollary}}
\newtheorem{conj}[thm]{{\color{blue}Conjecture}}
\newtheorem{defn}[thm]{{\color{blue}Definition}}
\newtheorem{prob}[thm]{{\color{blue}Problem}}
\newtheorem{lem}[thm]{{\color{blue}Lemma}}
\newcommand{\ve}[1]{\mathbf{#1}}
\newcommand{\vect}[1]{\begin{bmatrix}#1 \end{bmatrix}}
\newcommand{\veA}[1]{\overset{\rightharpoonup}{#1}}

\newcommand{\no}{\noindent}

\useoutertheme{miniframes}
\usefonttheme{serif}
\setbeamertemplate{headline}{}

%------------- title --------------------

\title{Berge Hypergraphs}

\author{Santiago Salazar}
\date{October 4, 2016}

\begin{document}

\frame{\titlepage}
%------------- 2 --------------------


\section{Introduction}

\frame{\frametitle{Introduction}
	The results in this paper come from work done this summer with Richard Anstee. The motivation for studying this problem came from Anstee's work in forbidden configurations as well as Dani\'el Gerbner and Corey Palmers' paper \emph{Extremal Results for Berge-Hypergraphs}.
}

\section{Notation}

\frame{\frametitle{Notation}

	\begin{itemize}

		\item<1-> Let {\color{red}$||A||$} denote the number of columns in a matrix $A$.

		\item<2-> For a $k_1\times \ell_1$ matrix $A$ and a $k_2\times \ell_2$ matrix $B$ denote {\color{red}$A\times B$ } as the $(k_1+k_2)\times (\ell_1\ell_2)$ matrix containing every columns of $A$ above every column of $B$.
		\item<3-> Let {\color{red}$\1_a\0_b$} denote the column of $a$ 1's above $b$ 0's. If $a$ of $b$ are 0 we write $\1_a$ or $\0_b$ instead.
		\item<4-> Let \red{$K_k$} denote the (0,1) $k$-rowed matrix containing all distinct columns.
			\smallskip
			eg.
			\[K_3 = \begin{bmatrix}
					1 & 1 & 1 & 0 & 1 & 0 & 0 & 0 \\
					1 & 1 & 0 & 1 & 0 & 1 & 0 & 0 \\
					1 & 0 & 1 & 1 & 0 & 0 & 1 & 0 \\
			\end{bmatrix}\]


	\end{itemize}
}

\section{Definitions}

\frame{\frametitle{Definitions}
	\begin{itemize}
		\item We say a matrix $A$ is \red{\emph{simple}} if it is a (0,1) matrix with no repeated columns. \pause
		\item Let $F$ and $A$ be two (0,1) matrices. We say that \red{$A$ \emph{contains} $F$ as a \emph{Berge hypergraph}} if there exists a submatrix of $A$, and then a row and column permutation of that matrix, call this matrix $G$, such that $F\leq G$. In this case we write \red{$F\bs G$} \pause
		\item Let \red{$\avbh(m, F)$} be the set
				\[\avbh(m, F) = \{ A : A\textrm{ is $m$-rowed, simple, } F\not\bs A \}.\] \pause
		\item Define the extremal function \red{$\bh(m, F)$} as 
			\[\bh(m, F) = \max_A\{ ||A|| : A \in \avbh(m,F)\}.\] 
	\end{itemize}
}
  

%% Maybe relation to patterns and forbidden configurations.
\section{Berge Hypergraph example}

\begin{frame}
	\frametitle{Berge Hypergraph Example}

	\[F = \begin{bmatrix} 1 & 1 & 1 \\ 0 & 1 & 0 \end{bmatrix}\;\;\;\;\; A = \begin{bmatrix} 
	\alert<2->{1} & 0 & \alert<2->{1} & \alert<2->{1} \\ 
	\alert<2->{1} & 1 & \alert<2->{0} & \alert<2->{1} \\
	0 & 1 & 0 & 1 \\
	1 & 0 & 1 & 0 \\   \end{bmatrix}\]

	\uncover<3->{\[\begin{bmatrix} 1 & 1 & 1 \\ 0 & 1 & 0 \end{bmatrix}
	\leq
	\begin{bmatrix} 1 & 1 & 1  \\ 1 & 1 & 0 \end{bmatrix}\]
	\bigskip
	\[ F\bs A\]
}


\end{frame}

\section{Identity Example}

\frame{\frametitle{Example $\bh(m,I_k)$}
	\begin{thm}
		Let $k$ be given and assume $m\geq k-1$. Then $\bh(m,I_k) = 2^{k-1}$.
	\end{thm}
	\pause
	\proof 

	We prove the upper bound, $\bh(m,I_k) \leq 2^{k-1}$ by induction on $k$.

	\medskip

	Base Case:
	Let $k=1$, then $I_1 = [1]$ and $A\in \avbh(m,[1])$ can only be the column of zeros. So $\avbh(m,I_1) = 1 = 2^0$.

	\pause

	\medskip

	Induction step:
	Assume $A \in \avbh(m,I_k)$ and let $B$ be the matrix $A$ with rows of 0's removed. If $||B||\leq 2^{k-2}$ we are done so assume $||B||>2^{k-2}$. Also assume $B$ has atleast $k$ rows. Then $I_{k-1}\bs B$, so permute $B$ to the form
	\[ B = \left[ \begin{array}{c|c}
				C & D \\ \hline
				E & G \\
	\end{array}\right]\]
	where $I_{k-1} \bs E$ and $E$ is $(k-1)\times (k-1)$.
}

\frame{\frametitle{Example $\avbh(m,I_k)$ (cont.)}
	\[ B = \left[ \begin{array}{c|c}
				C & D \\ \hline
				E & G \\
	\end{array}\right]\]

	Note that $D$ is the matrix of zero's so $G$ must be simple. Also note that since $B$ has no nonzero rows, $C$ has a 1. Therefore $I_{k-1}\not\bs G$. By the induction assumption $||G||\leq 2^{k-2}$ and so $||B|| = ||E|| + ||G|| = k-1 + 2^{k-2} \leq 2^{k-1}$. This proves $\bh(m,F) \leq 2^{k-1}$. 

	\pause

	The construction $K_{k-1}\times \0_{m-(k-1)}$ is in $\avbh(m,I_k)$ and has $2^{k-1}$ columns. Thus $\bh(m,F) = 2^{k-1}$.
	\qed
}

\section{General Results}

\frame{\frametitle{General Results}
	\begin{itemize}

		\item Complete asymptotic classification of $\bh(m,F)$ for all $1,2,3,4$-rowed $F$. \pause
			

		\item Complete asymptotic classification of $\bh(m,F)$ all 5-rowed $F$ with the exception of 
	\[ I_1 \times I_2 \times I_2 = 
		\begin{bmatrix} 	
			1 & 1 & 1 & 1 \\
			1 & 1 & 0 & 0 \\
			0 & 0 & 1 & 1 \\
			1 & 0 & 1 & 0 \\
			0 & 1 & 0 & 1 \\
	\end{bmatrix}\] \pause
	
		\item Asymptotic classification of $\bh(m,F)$ where $F$ is the vertex-edge incidence matrix of a forest. 
	\end{itemize}
} 

\section{Downsets}

\begin{frame}
	\frametitle{Downsets}
	
	We use a shifting operation \red{$T_i(A)$} on $A$ where we remove all $1$'s on row $i$ that do not create a repeated column by their removal. This operation preserves the number of columns and simplicity of the matrix. Also if $F$ is not a Berge hypergraph of $A$ then it is not a Berge hypergraph of $T_i(A)$. We apply $T_m(T_{m-1}(\cdots T_1(A)\cdots))$ until we can no longer remove 1's.

	If we interpret the resulting matrix $\red{T(A)}$ as a set system ${\cal S}$ then it is a \red{downset}. That is to say, if $S\in {\cal S}$ and $S'\subset S$, then $S'\in {\cal S}$. For the matrix $T(A)$, if a columns has 1's on rows $r_1,r_2,\dots,r_t$, then $K_t$ is contained on those $t$ rows.

	\bigskip

	Conclusion: if $A\in \avbh(m,F)$, we can assume $A$ has the downset property!

	

\end{frame}

\section{Shifting Example}

\begin{frame}
	\frametitle{Shifting Example}

	\[\kbordermatrix{
					&   &   &   &   &   &   &   &   &   &   &   &  \\
		\only<2-3>{\rightarrow}& 1 & \only<1-2>{1} \only<3->{0} & \only<1-2>{1} \only<3->{0} &  \only<1-2>{1} \only<3->{0} & \only<1-2>{1} \only<3->{0}  & \only<1-2>{1} \only<3->{0}  & 0 & 0 & 0 & \only<1-2>{1} \only<3->{0}  & \only<1-2>{1} \only<3->{0}  & 1 \\
		\only<4-5>{\rightarrow}& 0 & \only<1-4>{1} \only<5->{0} & \only<1-4>{1} \only<5->{0}  & 0 & \only<1-4>{1} \only<5->{0}  & 1 & \only<1-4>{1} \only<5->{0}  & 0 & 0 & 0 & \only<1-4>{1} \only<5->{0}  & \only<1-4>{1} \only<5->{0} \\
		\only<6-7>{\rightarrow}& 1 & 0 & 0 & \only<1-6>{1} \only<7->{0}  & 0 & \only<1-6>{1} \only<7->{0} & \only<1-6>{1} \only<7->{0} & \only<1-6>{1} \only<7->{0}   & 1 & \only<1-6>{1} \only<7->{0}   & \only<1-6>{1} \only<7->{0}   & 0\\
		\only<8>{\rightarrow}& 0 & \color<12>{green}{1} & \color<12>{green}{1} & \color<12>{green}{1} & 0 & 0 & 0 & \color<12>{green}{1} & 0 & 0 & 0 & 0\\
		\only<9>{\rightarrow}& 0 & \color<12>{green}{1} & \color<12>{green}{1} & 0 & 0 & 0 & \color<12>{green}{1} & 0 & 0 & \color<12>{green}{1} & 0 & 0\\
		\uncover<10>{\rightarrow}& 0 & \color<12>{green}{1} & 0 & \color<12>{green}{1} & 0 & 0 & \color<12>{green}{1} & 0 & 0 & 0 & \color<12>{green}{1} & 0\\
	}\]

\end{frame}


\frame{\frametitle{Graph Theory Machinery}
	\pause
	\begin{thm}
		Let $G$ be a simple graph where each vertex has degree greater than or equal to $k-1$. Then $G$ contains every tree on $k$ vertices as a subgraph.
	\end{thm}
	\pause

	\smallskip

	Let $\ex(m,K_k,G)$ denote the maximum number of subgraphs $K_k$ in an $m$-vertex graph avoiding $G$.
	\smallskip
	\pause

Let \red{$G(A)$} denote the $m$ vertex graph with vertices $i,j$ connected if and only if rows $i,j$ of $A$ have a $\left[\begin{smallmatrix} 1 \\ 1 \end{smallmatrix}\right]$ pair.
	\pause

	\emph{eg.}
	\[A = \begin{bmatrix}
			1 & 0 & 0 & 1  \\
			1 & 1 & 0 & 1  \\
			0 & 1 & 0 & 0  \\
			1 & 0 & 1 & 0  \\
			0 & 0 & 1 & 0  \\
	\end{bmatrix}\]


}

\section{$F$ is a tree}

\frame{\frametitle{$F$ is a tree}
	\begin{thm}
		Let $F$ be the vertex-edge incidence  matrix of a tree (or forest) $T$ on $k$ vertices. Then $\bh(m,F)$ is $\Theta(m)$.
	\end{thm}
	
	\pause

	\proof 
	
	Let $A\in \avbh(m,F)$ and assume $A$ is a downset. For each row $r$ of $A$ with column sum $2^{k-2}$ or less remove that row and the columns of $A$ with 1's on row $r$. This corresponds to at most $2^{k-2}m$ column deletions. Any rows left in $B$ have row sum strictly larger than $2^{k-2}$.  Note that this implies that $B$ has $k$ or more rows since $K_{k-1}$ has row sum $2^{k-2}$. Consider the submatrix $B_q$ of $B$ formed by taking the columns with 1's on row $q$ and taking every row but row $q$. $B_q$ is simple with $||B_q|| >  2^{k-2}$ and therefore contains $I_{k-1}$. Therefore the vertex $q$ in $G(B)$ will have degree $k-1$ or greater.
	
}

\begin{frame}
	\frametitle{$F$ is a tree (cont.)}
	By the theorem, $T$ is a subgraph in $G(B)$ and therefore $F$ is in the downset of $B$. Since the downset of $B$ is in $A$, $F$ is in $A$. This contradicts the hypothesis so we conclude that $A$ has fewer than $2^{k-2}m$ rows. 
	\medskip
	\pause

	The lower bound follows from the construction $I_m$. \qed

\end{frame}


\section{Complete Bipartite}

\begin{frame}
	\frametitle{Avoiding the Complete Bipartite Graph}	
	Let \red{$K_{s,t}$} denote the complete bipartite graph on $s$ and $t$ vertices. The vertex-edge incidince matrix of $K_{s,t}$ is $I_s\times I_t$. We use the following theorems to prove results about $\bh(m,I_s\times I_t)$.
	\begin{thm} W. G. Brown (1966) 
	
		For $t\geq 2$, $\ex(m,K_{2,t})$ is $\Theta(m^\frac{3}{2})$.
	\end{thm} 

	\begin{thm} N. Alon, L. R\'onyai, T. Szab\'o (1999)
		
		For $t\geq 3$, $\ex(m,K_{3,t})$ is $\Theta(m^\frac{5}{3})$.
	\end{thm}

	\pause

	\begin{thm} For $t\geq 2$, $\bh(m,I_2\times I_t)$ is $\Theta(m^\frac{3}{2})$.
	\end{thm}

	\begin{thm}
		For $t\geq 3$, $\bh(m,I_3\times I_t)$ is $\Theta(m^2)$.
	\end{thm}

\end{frame}

\section{Column sum restriction}


\begin{frame}
		\frametitle{Column Sum Restriction}
			For general $I_s\times I_t$, we consider matrices with column sums $1,2,\dots,s$. Suppose $A\in\avbh(m,F)$ has a column $\alpha$ with column sum $s$. The number of columns $\beta_i$ with $\beta_i>\alpha$ is bounded by $2^{t-1}$.
			\[\kbordermatrix{ 
				& \alpha & \beta_1 &\beta_2 & 2^{t-1}+1 & \beta_{n}\\
				& 1 & 1 & 1& \cdots & 1 \\
				s& \vdots & \vdots &\vdots & \vdots & \vdots \\
				& 1 & 1 & 1 & \cdots & 1 \\ 
				\\
				\\
				&  &   &  B & & \\
				\\
				\\ }\]
						 
\end{frame}


\begin{frame}
	\frametitle{Column Sum Restriction (cont.)}
	Note that $B$ is a simple matrix with $||B|| > \bh(m,I_t)$ so $I_t \bs B$. We apply the downset idea and note that we can find $I_s \times I_t$ in $A$.
	\[\begin{bmatrix}
	1 & 1 & \cdots & 1 \\
	\vdots &\vdots & \vdots & \vdots \\
	1 & 1 &  \cdots & 1 \\ 
	1 &   &         &   \\
	  &  1  &       &   \\
	&   &  \ddots       &   \\
	&    &         & 1  \\ 
		\end{bmatrix} \uncover<2->{\Rightarrow \begin{bmatrix} 
			1 	& \cdots & 1 	 & 0   	  & \cdots & 0 & 0 & \cdots & 0\\ 
			0 	& \cdots & 0  	 & 1 	  & \cdots & 1 & 0 &\cdots &0\\ 
			\vdots  & \vdots & \vdots& \vdots & \vdots & \vdots &\vdots &\vdots & \vdots\\ 
			0 	& \cdots & 0  	 & 0 	  & \cdots & 0 & 0 &\cdots &1\\
			1	&       &        &1       &        &   & 1  &       & \\
			& \ddots&        &        & \ddots &   &   &  \cdots     & \\
			&       & 1       &        &        & 1  &   &       &  1
	\end{bmatrix}} \] 

	\uncover<3->{ We therefore restrict ourselves to considering the columns of column sum $s$ or less since the number of columns of larger column sum is bounded by a constant times the number of columns of column sum $s$.}
\end{frame}

\section{$I_2\times I_t$}
\begin{frame}
	\frametitle{$\bh(m,I_2\times I_t)$}

	\begin{thm} Let $F=I_2\times I_t$ be the vertex-edge incidence matrix of the complete bipartite graph $K_{2,t}$. Then $\bh(m,F)$ is $\Theta(\ex(m,K_{2,t}))$ which is $\Theta(m^\frac{3}{2})$
	\end{thm}
	\pause
	
	\proof Let $A \in\avbh(m,I_2\times I_t)$ and assume $A$ has column sums at most 2. The number of columns of column sum 0 or 1 is bounded by $m+1$ and the number of columns of column sum 2 is bounded by $2^{t-1}\ex(m,K_{2,t})$. It is known that $\ex(m,K_{2,t})$ is $\Theta(m^\frac{3}{2})$. Thus $\bh(m,I_2\times I_t) \leq 2^{t-1}\ex(m,K_{2,t}) + m+1$ which is $O(m^\frac{3}{2})$.  \medskip
	
	It follows from the existence of a graph with $\Theta(m^\frac{3}{2})$ edges that we can take the corresponding vertex-edge incidence matrix and get the lower bound. Therefore $\bh(m,I_2\times I_t)$ is $\Theta(m^\frac{3}{2})$. \qed 


	\end{frame} 

	\begin{frame} 
		\frametitle{Graph reduction} 
		
		We cannot use the same approach to determine $\bh(m,I_3\times I_t)$  since we must consider edges of size 3. However, we can still reduce $\bh(m,I_3\times I_t)$ to a graph theory problem. Let $A\in\avbh(m,I_3\times I_t)$ and let $A$ have the downset property. If $A$ has a column of sum 3 on rows $i,j,k$, then the vertices $i,j,k$ in $G(A)$ have a triangle. Therefore the number of columns of sum 3 in $A$ is bounded by $\ex(m,K_3,K_{s,t})$. Conversely, we can show that if we have a triangle on rows $i,j,k$ of $A$, then we can have a column with 1's on those rows. Suppose that on rows $i,j,k$ of $A\in\avbh(m,I_3\times I_t)$ we have a triangle $K_3$. Append the column $\alpha$ with 1's on $i,j,k$ and 0's elsewhere. Call the new matrix $B$. 
	\end{frame}

	\begin{frame}
		\frametitle{Graph reduction (cont)}
	 
		\[\kbordermatrix{ 
		& a & b & c & \alpha \\ 
		i & 1 & 1 & 0 & 1\\
		j & 1 & 0 & 1 & 1\\
		k & 0 & 1 & 1 & 1\\ }
	\]
	Now suppose the forbidden object has been formed in $B$. Since it was not in $A$, column $\alpha$ must be part of the submatrix. Furthermore, since there are two 1's in $I_3\times I_t$ two of rows $i,j, k $ must also be part of the submatrix. Suppose without loss of generality, that those rows are $i,j$. We note that column $a$ can not be in the submatrix since that would form a $2\times 2 $ submatrix of 1's. However, that implies that we could equivalently take $a$ instead of $\alpha$ in the submatrix. Therefore the forbidden object is in $A$, a contradiction. We conclude that the forbidden object is not in $B$.
\end{frame}

\section{I_3 X I_t}

\begin{frame}
	\frametitle{$\bh(m,I_3\times I_t)$ Upper bound}
	\begin{lem}
		\label{triangle}
		N. Alon, C. Shikhelman (2015)

		For any fixed $s\geq 2$ and $t\geq (s-1)! + 1, \ex(m,K_3,K_{s,t})$ is $\Theta(m^{3-\frac{3}{s}})$.

	\end{lem}

	\begin{thm} 
		For $t\geq 3$, $\bh(m,I_3\times I_t)$ is $\Theta(m^2)$.
	\end{thm}
	\pause

	\proof We consider columns of column sum 3 or less. The number of columns with column sum 2 or less is bounded by $\binom{m}{2} + \binom{m}{1} + \binom{m}{0}$. As we showed before, the number of columns of column sum 3 is bounded by $\ex(m,K_3,K_{3,t})$ which is $\Theta(m^2)$.  Therefore we have that $\bh(m,I_3\times I_t)$ is $O(m^2)$. 
\end{frame}


\begin{frame}
	\frametitle{$\bh(m,I_3\times I_t)$ Lower bound}

For the lower bound we take the construction, $G$, used in the lemma and construct a matrix with a column of column sum 3 on rows $i,j,k$ if vertices $i,j,k$ of $G$ have a triangle. As we showed, this new matrix avoids $I_3\times I_t$, is simple, and has $\Theta(m^2)$ columns. Therefore $\bh(m,I_3\times I_t)$ is $\Theta(m^2)$ \qed


\end{frame}

\section{General I_s X I_t}

\begin{frame}
	\frametitle{General $I_s \times I_t$}

	Although the bounds we have found are for $s=2$ and $s=3$, our methods generalize to $I_s\times I_t$. For any $s$ and $t\geq s$,  we have that
	\[\bh(m,I_s\times I_t) \textrm{ is } \Theta\left(\sum_{i=0}^s \ex(m,K_i, K_{s,t})\right)\]

	\pause
	Alon and Shikhelman's work is particularly relevant as can be seen by the following theorem.
	
	\begin{thm}
		For any fixed $r,s\geq 2r-2$, and $t\geq (s-1)! +1$. Then,
		\[ \ex(m,K_r,K_{s,t}) \geq \left(\frac{1}{r!} + o(1)\right)m^{r-\frac{r(r-1)}{s}}. \]
	\end{thm}
\end{frame}
      

\begin{frame}
	\frametitle{}
	\begin{center}
		\huge Thanks for listening!
	\end{center}
\end{frame}

\end{document}
